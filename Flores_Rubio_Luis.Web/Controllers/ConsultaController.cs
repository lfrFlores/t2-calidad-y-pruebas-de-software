﻿using Flores_Rubio_Luis.Domain;
using Flores_Rubio_Luis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Flores_Rubio_Luis.Web.Controllers
{
    public class ConsultaController : Controller
    {

        IGenericService<Paciente> _pacientes;
        IGenericService<Especie> _especies;
        IGenericService<Cliente> _clientes;
        IGenericService<Consulta> _consultas;

        public ConsultaController(IGenericService<Paciente> _pacientes, IGenericService<Especie> _especies, IGenericService<Cliente> _clientes, IGenericService<Consulta> _consultas)
        {
            this._pacientes = _pacientes;
            this._especies = _especies;
            this._clientes = _clientes;
            this._consultas = _consultas;
        }

        public ActionResult GetByPacienteId(int id)
        {
            return View(_consultas.Find(x => x.PacienteId == id));
        }

        public ActionResult Create(int id)
        {
            return View(
                new Consulta
                {
                    Fecha = DateTime.Now,
                    PacienteId = id
                }
            );
        }

        [HttpPost]
        public ActionResult Create(Consulta model)
        {
            var txtRequerido = "Campo Requerido";


            if (model.Fecha > DateTime.Now)
                ViewData.ModelState.AddModelError("Fecha", "Fecha no válida");

            if (string.IsNullOrEmpty(model.Fecha.ToString()))
                ViewData.ModelState.AddModelError("Fecha", txtRequerido);


            if (string.IsNullOrEmpty(model.Motivo))
                ViewData.ModelState.AddModelError("Motivo", txtRequerido);


            if (model.Peso <= 0)
                ViewData.ModelState.AddModelError("Peso", "Inválido");


            if (!string.IsNullOrEmpty(model.Comentarios))
                if (model.Comentarios.Length > 200)
                    ViewData.ModelState.AddModelError("Comentarios", "Demasiado largo");

            if (ModelState.IsValid)
            {
                if (model.Peso > 0)
                {
                    var paciente = _pacientes.GetById(model.PacienteId);
                    paciente.Peso = model.Peso;
                    _pacientes.Edit(paciente);
                }

                _consultas.Add(model);
                return RedirectToAction("Index", "Paciente");
            }

            return View(model);
        }
    }
}