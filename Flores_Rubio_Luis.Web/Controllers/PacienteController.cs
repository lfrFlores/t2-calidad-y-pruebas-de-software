﻿using Flores_Rubio_Luis.Domain;
using Flores_Rubio_Luis.Repository;
using Flores_Rubio_Luis.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Flores_Rubio_Luis.Web.Controllers
{
    public class PacienteController : Controller
    {
        IGenericService<Paciente> _servicePaciente;
        IGenericService<Cliente> _serviceCliente;
        IGenericService<Especie> _serviceEspecie;
        IGenericService<Sexo> _serviceSexo;

        public PacienteController(IGenericService<Paciente> _servicePaciente, 
            IGenericService<Cliente> _serviceCliente, 
            IGenericService<Especie> _serviceEspecie, 
            IGenericService<Sexo> _serviceSexo)
        {
            this._servicePaciente = _servicePaciente;
            this._serviceCliente = _serviceCliente;
            this._serviceEspecie = _serviceEspecie;
            this._serviceSexo = _serviceSexo;
        }


        // GET: Paciente
        public ActionResult Index()
        {
            return View(_servicePaciente.GetAll(x => x.cliente,c=>c.especie,v=>v.sexo));
        }



        public ActionResult Create()
        {
            

            ViewBag.Titulo = "Registrar";
            ViewBag.Clientes = _serviceCliente.GetAll();
            ViewBag.Especies = _serviceEspecie.GetAll();
            ViewBag.Sexos = _serviceSexo.GetAll();
            return View();
        }


        [HttpPost]
        public ActionResult Create(Paciente model)
        {
            var txtRequerido = "Campo Requerido";


             
            if (model.nro_Registro < 1)
                ViewData.ModelState.AddModelError("nro_Registro", "Mayor que 0");

            if (string.IsNullOrEmpty(model.nro_Registro.ToString()))
                ViewData.ModelState.AddModelError("nro_Registro", txtRequerido);

            if (_servicePaciente.Find(x => x.nro_Registro == model.nro_Registro).Count() > 0)
                ViewData.ModelState.AddModelError("nro_Registro", "Ya existe");


            if (model.fecha_Registro > DateTime.Now)
                ViewData.ModelState.AddModelError("fecha_Registro", "Fecha Proxima");

            if (string.IsNullOrEmpty(model.fecha_Registro.ToString()))
                ViewData.ModelState.AddModelError("fecha_Registro", txtRequerido);


            if (string.IsNullOrEmpty(model.Id_Cliente.ToString()))
                ViewData.ModelState.AddModelError("Id_Cliente", txtRequerido);


            if (string.IsNullOrEmpty(model.Nombre_Paciente))
                ViewData.ModelState.AddModelError("Nombre_Paciente", txtRequerido);


            if (string.IsNullOrEmpty(model.Id_Especie.ToString()))
                ViewData.ModelState.AddModelError("Id_Especie", txtRequerido);


            if (model.Peso <= 0)
                ViewData.ModelState.AddModelError("Peso", "Debe ser mayor que 0");


            if (!string.IsNullOrEmpty(model.Particulares) && model.Particulares.Length > 200)
                ViewData.ModelState.AddModelError("Particulares", "Demasiado largo");

            if (ModelState.IsValid)
            {
                _servicePaciente.Add(model);
                return RedirectToAction("Index");
            }

            ViewBag.Clientes = _serviceCliente.GetAll();
            ViewBag.Especies = _serviceEspecie.GetAll();
            ViewBag.Sexos = _serviceSexo.GetAll();

            return View(model);
        }

    }
}