﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flores_Rubio_Luis.Domain
{
    public class Cliente
    {
        [Key]
        public int Id_Cliente { get; set; }
        public String Nombre { get; set; }
    }
}
