﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flores_Rubio_Luis.Domain
{
    public class Consulta
    {
        public int ConsultaId { get; set; }
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime Fecha { get; set; }
        public string Motivo { get; set; }
        public int Peso { get; set; }
        public string Comentarios { get; set; }
        public int PacienteId { get; set; }
        public Paciente Paciente { get; set; }
    }
}
