﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flores_Rubio_Luis.Domain
{
    public class Paciente
    {
        [Key]
        public int Id_Paciente { get; set; }
        public int nro_Registro { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]
        public DateTime fecha_Registro { get; set; }

        public int Id_Cliente { get; set; }
        public Cliente cliente { get; set; }


        public String Nombre_Paciente { get; set; }

        public int Id_Sexo { get; set; }
        public Sexo sexo { get; set; }


        public int Id_Especie { get; set; }
        public Especie especie { get; set; }


        public float Peso { get; set; }
        public String Particulares { get; set; }
    }
}
