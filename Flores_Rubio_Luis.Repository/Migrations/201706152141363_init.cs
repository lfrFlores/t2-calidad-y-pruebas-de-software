namespace Flores_Rubio_Luis.Repository.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cliente",
                c => new
                    {
                        Id_Cliente = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id_Cliente);
            
            CreateTable(
                "dbo.Consulta",
                c => new
                    {
                        ConsultaId = c.Int(nullable: false, identity: true),
                        Fecha = c.DateTime(nullable: false),
                        Motivo = c.String(maxLength: 4000),
                        Peso = c.Int(nullable: false),
                        Comentarios = c.String(maxLength: 4000),
                        PacienteId = c.Int(nullable: false),
                        Paciente_Id_Paciente = c.Int(),
                    })
                .PrimaryKey(t => t.ConsultaId)
                .ForeignKey("dbo.Paciente", t => t.Paciente_Id_Paciente)
                .Index(t => t.Paciente_Id_Paciente);
            
            CreateTable(
                "dbo.Paciente",
                c => new
                    {
                        Id_Paciente = c.Int(nullable: false, identity: true),
                        nro_Registro = c.Int(nullable: false),
                        fecha_Registro = c.DateTime(nullable: false),
                        Id_Cliente = c.Int(nullable: false),
                        Nombre_Paciente = c.String(maxLength: 4000),
                        Id_Sexo = c.Int(nullable: false),
                        Id_Especie = c.Int(nullable: false),
                        Peso = c.Single(nullable: false),
                        Particulares = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id_Paciente)
                .ForeignKey("dbo.Cliente", t => t.Id_Cliente)
                .ForeignKey("dbo.Especie", t => t.Id_Especie)
                .ForeignKey("dbo.Sexo", t => t.Id_Sexo)
                .Index(t => t.Id_Cliente)
                .Index(t => t.Id_Sexo)
                .Index(t => t.Id_Especie);
            
            CreateTable(
                "dbo.Especie",
                c => new
                    {
                        Id_Especie = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id_Especie);
            
            CreateTable(
                "dbo.Sexo",
                c => new
                    {
                        Id_Sexo = c.Int(nullable: false, identity: true),
                        Nombre = c.String(maxLength: 4000),
                    })
                .PrimaryKey(t => t.Id_Sexo);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Consulta", "Paciente_Id_Paciente", "dbo.Paciente");
            DropForeignKey("dbo.Paciente", "Id_Sexo", "dbo.Sexo");
            DropForeignKey("dbo.Paciente", "Id_Especie", "dbo.Especie");
            DropForeignKey("dbo.Paciente", "Id_Cliente", "dbo.Cliente");
            DropIndex("dbo.Paciente", new[] { "Id_Especie" });
            DropIndex("dbo.Paciente", new[] { "Id_Sexo" });
            DropIndex("dbo.Paciente", new[] { "Id_Cliente" });
            DropIndex("dbo.Consulta", new[] { "Paciente_Id_Paciente" });
            DropTable("dbo.Sexo");
            DropTable("dbo.Especie");
            DropTable("dbo.Paciente");
            DropTable("dbo.Consulta");
            DropTable("dbo.Cliente");
        }
    }
}
