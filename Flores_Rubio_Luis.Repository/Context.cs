﻿using Flores_Rubio_Luis.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flores_Rubio_Luis.Repository
{
    public class Context: DbContext
    {
        public DbSet<Paciente> Registros { get; set; }
        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Especie> Especies { get; set; }
        public DbSet<Sexo> Sexos { get; set; }
        public DbSet<Consulta> Consultas { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}
