﻿using Flores_Rubio_Luis.Domain;
using Flores_Rubio_Luis.Service;
using Flores_Rubio_Luis.Web.Controllers;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Flores_Rubio_Luis.RepositoryTest
{
    [Category("Paciente")]
    [TestFixture]
    class MyMaskotTest_Paciente
    {
        Mock<IGenericService<Paciente>> _pacientes;
        Mock<IGenericService<Especie>> _especies;
        Mock<IGenericService<Cliente>> _clientes;
        Mock<IGenericService<Sexo>> _sexos;

        [SetUp]
        public void Inicial()
        {
            _pacientes = new Mock<IGenericService<Paciente>>();
            _especies = new Mock<IGenericService<Especie>>();
            _clientes = new Mock<IGenericService<Cliente>>();
            _sexos = new Mock<IGenericService<Sexo>>();
        }



        [Test]
        public void PuedoVerFormularioDeCrearPaciente()
        {
            var controller = new PacienteController(
                           _pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);

            var resultado = controller.Create(
                new Paciente
                {
                    fecha_Registro = DateTime.Parse("2017/06/25")
                }) as ViewResult;
        }


        [Test]
        public void GuardarPaciente_nro_registro_mayora0()
        {
            var controller = new PacienteController(
                           _pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);



            controller.Create(
                new Paciente
                {
                    nro_Registro = 0
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("nro_Registro"));
        }


        [Test]
        public void ListaPacientes()
        {
            _pacientes.Setup(o => o.GetAll(x => x.cliente, c => c.especie, v => v.sexo)).Returns( //It.IsAny<Persona>()
                new List<Paciente>()
                {
                    new Paciente {
                    },
                    new Paciente
                    {
                    }
                }
            );

            var controller = new PacienteController(
                _pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);

            var resultado = controller.Index() as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Paciente>).Count());
        }

        [Test]
        public void GuardarPaciente_FechaProxima()
        {
            var controller = new PacienteController(_pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);
            controller.Create(
                new Paciente
                {
                    fecha_Registro = DateTime.Parse("2017/06/25")
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("fecha_Registro"));
        }
      
        [Test]
        public void GuardarPaciente_NombreRequired()
        {
            var controller = new PacienteController(_pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);
            controller.Create(
                new Paciente
                {

                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Nombre_Paciente"));
        }

        [Test]
        public void GuardarPaciente_nro_registro_Required()
        {
            var controller = new PacienteController(_pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);
            controller.Create(
                new Paciente
                {

                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("nro_Registro"));
        }



        [Test]
        public void GuardarPaciente_PesoMayorCero()
        {
            var controller = new PacienteController(_pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);
            controller.Create(
                new Paciente
                {
                    Peso = -1
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Peso"));
        }

        [Test]
        public void GuardarPaciente_DatosParticularesLength()
        {
            var controller = new PacienteController(_pacientes.Object, _clientes.Object, _especies.Object, _sexos.Object);
            controller.Create(
                new Paciente
                {
                    Particulares = "La key ClientValidationEnabled en true nos permite trabajar con la validación del lado del cliente de manera predetermina. La key UnobtrusiveJavaScriptEnable en true nos evitara mezclar el código javascript dentro del HTML, tal como se explico en este post."
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Particulares"));
        }

    }
    [Category("Consulta")]
    [TestFixture]
    class MyMaskotTest_Consulta
    {
        Mock<IGenericService<Paciente>> _pacientes;
        Mock<IGenericService<Especie>> _especies;
        Mock<IGenericService<Cliente>> _clientes;
        Mock<IGenericService<Consulta>> _consultas;
        private ConsultaController controllerconsulta;

        [SetUp]
        public void Inicial()
        {
            _pacientes = new Mock<IGenericService<Paciente>>();
            _especies = new Mock<IGenericService<Especie>>();
            _clientes = new Mock<IGenericService<Cliente>>();
            _consultas = new Mock<IGenericService<Consulta>>();
        }
        
        [Test]
        public void PuedoVerFormularioDeCrearConsulta()
        {
            var controller = new ConsultaController(
                           _pacientes.Object,
                           _especies.Object,
                           _clientes.Object,
                           _consultas.Object);

            var resultado = controller.GetByPacienteId(1) as ViewResult;
        }

        private ActionResult PublicarConsulta(Consulta consulta)
        {
            return controllerconsulta.Create(consulta);
        }

        [Test]
        public void ListaConsultas()
        {

            //_consultas.Setup(o => o.Find(x=>x.PacienteId==1)).Returns( //It.IsAny<Persona>()
            _consultas.Setup(o => o.Find(It.IsAny<Expression<Func<Consulta, bool>>>())).Returns( //It.IsAny<Persona>()
                new List<Consulta>()
                {
                    new Consulta {
                    },
                    new Consulta
                    {
                    }
                }
            );

            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            var resultado = controller.GetByPacienteId(1) as ViewResult;
            Assert.AreEqual(2, (resultado.Model as List<Consulta>).Count());
        }

        [Test]
        public void Guardar_Consulta_FechaProxima()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    Fecha = DateTime.Parse("2017/06/25")
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Fecha"));
        }

        [Test]
        public void Guardar_Consulta_Motivo_Required()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {

                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Motivo"));
        }

        [Test]
        public void GuardarConsulta_PesoMayorCero()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    Peso = -1
                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Peso"));
        }
        [Test]
        public void GuardarConsulta_ComentarioLength()
        {
            var controller = new ConsultaController(
                _pacientes.Object,
                _especies.Object,
                _clientes.Object,
                _consultas.Object);

            controller.Create(
                new Consulta
                {
                    Comentarios = "La key ClientValidationEnabled en true nos permite trabajar con la validación del lado del cliente de manera predetermina. La key UnobtrusiveJavaScriptEnable en true nos evitara mezclar el código javascript dentro del HTML, tal como se explico en este post."

                }
            );
            Assert.AreEqual(false, controller.ViewData.ModelState.IsValid);
            Assert.True(controller.ViewData.ModelState.ContainsKey("Comentarios"));
        }

       
    }
}
